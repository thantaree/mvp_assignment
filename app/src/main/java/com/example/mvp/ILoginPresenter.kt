package com.example.mvp

import android.text.Editable

interface ILoginPresenter {

    fun onSubmitLogin(name: String, password:String)
    fun onStart()

}