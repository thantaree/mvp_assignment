package com.example.mvp

interface ILoginView {

    fun showLayoutScreen()
    fun showPopupLoginSuccess()
    fun showErrorLine()

}