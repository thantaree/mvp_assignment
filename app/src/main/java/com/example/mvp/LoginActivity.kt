package com.example.mvp

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.transition.Slide
import android.transition.TransitionManager
import android.view.Gravity
import android.view.Gravity.END
import android.view.LayoutInflater
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class LoginActivity : AppCompatActivity(), ILoginView {

    lateinit var loginPresenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var name = this.findViewById<EditText>(R.id.et_name)
        var password = this.findViewById<EditText>(R.id.et_password)
        var submitbtn = this.findViewById<Button>(R.id.btn_submit)
        var clearbtn = this.findViewById<Button>(R.id.btn_clear)

        submitbtn.setOnClickListener {

            loginPresenter.onSubmitLogin(name.text.toString(), password.text.toString())
        }

        clearbtn.setOnClickListener {
            et_name.text.clear()
            et_password.text.clear()
            tv_error.text = ""
        }

        loginPresenter = LoginPresenter(this)
        loginPresenter.onStart()


    }

    override fun showLayoutScreen() {
        Toast.makeText(applicationContext, "Hello", Toast.LENGTH_LONG).show()
    }

    @SuppressLint("InflateParams")
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun showPopupLoginSuccess() {

        val inflater: LayoutInflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(
            R.layout.activity_popup,
            null
        )

        val popupWindow = PopupWindow(
            view, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            popupWindow.enterTransition = slideIn

            val slideOut = Slide()
            slideOut.slideEdge = END
            popupWindow.exitTransition = slideOut

        }

        var popbtn = view.findViewById<Button>(R.id.btn_popup)
        popbtn.setOnClickListener {
            popupWindow.dismiss()
        }
        popupWindow.setOnDismissListener {
            Toast.makeText(applicationContext, "Popup closed", Toast.LENGTH_SHORT).show()
        }

        TransitionManager.beginDelayedTransition(main)
        popupWindow.showAtLocation(main, Gravity.CENTER, 0, 0)
    }

    override fun showErrorLine() {
        tv_error.text = getString(R.string.login_failed)
    }


}


