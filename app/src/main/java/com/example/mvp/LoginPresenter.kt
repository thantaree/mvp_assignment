package com.example.mvp


class LoginPresenter(val view: ILoginView) : ILoginPresenter {

    override fun onSubmitLogin(name: String, password: String) {
        //user = "Muk" password = 1234
        if (name == "Muk" && password == "1234") {
            view.showPopupLoginSuccess()
        }else{
            view.showErrorLine()
        }
    }

    override fun onStart() {
        view.showLayoutScreen()
    }

}